import 'dart:convert';
import 'package:flutter/material.dart';
import 'api.dart';
import 'user.dart';

void main() => runApp(ListApp());

class ListApp extends StatelessWidget {
  @override
  build(context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'My Http App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyListScreen(),
    );
  }
}

class MyListScreen extends StatefulWidget {
  @override
  createState() => _MyListScreenState();
}

class _MyListScreenState extends State {
  var users = <User>[];

  _getUsers() {
    Api.getUsers().then((response) {
      setState(() {
        Iterable list = json.decode(response.body);
        users = list.map((model) => User.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getUsers();
  }

  dispose() {
    super.dispose();
  }

  @override
  build(context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("User List"),
        ),
        body: ListView.builder(
          itemCount: users.take(5).length,
          itemBuilder: (context, index) {
            return ListTile(title: Center(child: Text(users[index].name)));
          },
        ));
  }
}
